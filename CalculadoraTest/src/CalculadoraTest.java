import org.junit.Assert;
import org.junit.Test;

import bsi.calculadora.CalculadoraImpl;
import bsi.calculadora.ICalculadora;


public class CalculadoraTest {

	private ICalculadora calculadora;
	
	@Test
	public void test() {

		calculadora = new CalculadoraImpl();
		
		String soma = calculadora.soma("I", "I");
		Assert.assertEquals("II", soma);

		soma = calculadora.soma("II", "I");
		Assert.assertEquals("III", soma);

		soma = calculadora.soma("II", "II");
		Assert.assertEquals("IV", soma);

		soma = calculadora.soma("III", "II");
		Assert.assertEquals("V", soma);

		soma = calculadora.soma("III", "III");
		Assert.assertEquals("VI", soma);

		soma = calculadora.soma("IV", "II");
		Assert.assertEquals("VI", soma);

		soma = calculadora.soma("II", "IV");
		Assert.assertEquals("VI", soma);

		soma = calculadora.soma("V", "III");
		Assert.assertEquals("VIII", soma);
		
		soma = calculadora.soma("IV", "V");
		Assert.assertEquals("IX", soma);
	}

}
