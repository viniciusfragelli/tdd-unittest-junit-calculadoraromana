package bsi.calculadora;

public class CalculadoraImpl implements ICalculadora {

	@Override
	public String soma(String a, String b) {
		a = expand(a);
		b = expand(b);
		return compress(a+b);
	}
	
	private String compress(String origin)
	{
		String old = origin;
		String dnew = origin;
		while(true)
		{
			dnew = old.replaceFirst("IIII","IV");
			dnew = dnew.replaceFirst("IVI", "V");
			dnew = dnew.replaceFirst("VIV", "IX");
			if(old.equals(dnew))
				break;
			old = dnew;
		}
		
		return dnew;
	}
	
	private String expand(String origin)
	{
		String old = origin;
		String dnew = origin;
		while(true)
		{
			dnew = old.replace("IV","IIII");
			dnew = dnew.replace("V", "IIIII");
			dnew = dnew.replace("IX", "IIIIIIIII");
			if(old.equals(dnew))
				break;
			old = dnew;
		}
		
		return dnew;
		
	}
	
	

}
